package io.renren.modules.wedding.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.github.classgraph.json.Id;
import io.renren.common.WeddingDeviceTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-09-09
 */
@Data
@TableName("wedding_device")
public class WeddingDeviceEntity {

    /**
     * 
     */
    @TableId(type = IdType.AUTO)
	private Integer id;
    /**
     * 所属酒店
     */
	private String hotel;
    /**
     * 婚期
     */
	private String time;
    /**
     * 类型
     */
	private Integer type;
    /**
     * 厅
     */
	private String office;
    /**
     * 签单人
     */
	private String signer;
    /**
     * 场布结构
     */
	private String structureLayout;
    /**
     * 大屏/酒店音响
     */
	private String audioSystem;
    /**
     * 司仪
     */
	private String masterIfCeremonies;
    /**
     * 摄影/快剪
     */
	private String photograph;
    /**
     * 摇臂/管家
     */
	private String housekeeper;
    /**
     * 液氮
     */
	private String liquidNitrogen;
    /**
     * 总金额
     */
	private String totalAmount;
    /**
     * 定金
     */
	private String deposit;
    /**
     * 中期款
     */
	private String interimPayment;
    /**
     * 尾款
     */
	private String balancePayment;
    /**
     * 软装人工费+督导
     */
	private String costForSoft;
    /**
     * 新人信息
     */
	private String newcomerInformation;
    /**
     * 新人电话
     */
	private String newcomerPhoneNumber;
    /**
     * 备注
     */
	private String remark;
    /**
     * sheetName
     */
    private String sheetName;
    /**
     * 操作人ID
     */
    private Long operatorId;
    /**
     * 操作人ID
     */
    private Date createTime;
    /**
     * 操作人ID
     */
    private Date updateTime;
}