package io.renren.modules.wedding.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.security.user.SecurityUser;
import io.renren.modules.security.user.UserDetail;
import io.renren.modules.wedding.dto.WeddingDeviceDTO;
import io.renren.modules.wedding.excel.WeddingDeviceExcel;
import io.renren.modules.wedding.service.WeddingDeviceService;
import io.renren.modules.wedding.vo.req.WeddingDevicePageReqVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-09-09
 */
@RestController
@RequestMapping("/wedding/device")
@Api(tags="")
public class WeddingDeviceController {
    @Autowired
    private WeddingDeviceService weddingDeviceService;

    @PostMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
//    @RequiresPermissions("demo:weddingdevice:page")
    public Result<PageData<WeddingDeviceDTO>> page(@ApiIgnore @RequestBody WeddingDevicePageReqVo req){
        PageData<WeddingDeviceDTO> page = weddingDeviceService.getPage(req);
        return new Result<PageData<WeddingDeviceDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("weddingdevice:info")
    public Result<WeddingDeviceDTO> get(@PathVariable("id") Long id){
        WeddingDeviceDTO data = weddingDeviceService.get(id);

        return new Result<WeddingDeviceDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("weddingdevice:save")
    public Result save(@RequestBody WeddingDeviceDTO dto){
        UserDetail user = SecurityUser.getUser();

        dto.setOperatorId(user.getId());
        dto.setCreateTime(new Date());
        dto.setUpdateTime(new Date());

        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        weddingDeviceService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("weddingdevice:update")
    public Result update(@RequestBody WeddingDeviceDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        weddingDeviceService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("weddingdevice:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        weddingDeviceService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("weddingdevice:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        weddingDeviceService.exportExcel(params,response);
    }

    @PostMapping("import")
    @ApiOperation("导入")
    @LogOperation("导入")
    @RequiresPermissions("weddingdevice:importData")
    public Result importData(MultipartFile file) {
        weddingDeviceService.importData(file);
        return new Result();
    }

}