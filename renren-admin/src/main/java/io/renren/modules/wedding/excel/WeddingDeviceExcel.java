package io.renren.modules.wedding.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import io.renren.common.WeddingDeviceTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-09-09
 */
@Data
public class WeddingDeviceExcel implements Serializable {
    @Excel(name = "")
    private Integer id;
    @Excel(name = "所属酒店")
    private String hotel;
    @Excel(name = "婚期")
    private String time;
    @Excel(name = "类型", replace = {"午宴_0", "晚宴_1"})
    private Integer type;
    @Excel(name = "厅")
    private String office;
    @Excel(name = "签单人")
    private String signer;
    @Excel(name = "场布结构")
    private String structureLayout;
    @Excel(name = "大屏/酒店音响")
    private String audioSystem;
    @Excel(name = "司仪")
    private String masterIfCeremonies;
    @Excel(name = "摄影/快剪")
    private String photograph;
    @Excel(name = "摇臂/管家")
    private String housekeeper;
    @Excel(name = "液氮")
    private String liquidNitrogen;
    @Excel(name = "总金额")
    private String totalAmount;
    @Excel(name = "定金")
    private String deposit;
    @Excel(name = "中期款")
    private String interimPayment;
    @Excel(name = "尾款")
    private String balancePayment;
    @Excel(name = "软装人工费+督导")
    private String costForSoft;
    @Excel(name = "新人信息")
    private String newcomerInformation;
    @Excel(name = "新人电话")
    private String newcomerPhoneNumber;
    @Excel(name = "备注")
    private String remark;
    @Excel(name = "sheetName",isColumnHidden = true)
    private String sheetName;

}