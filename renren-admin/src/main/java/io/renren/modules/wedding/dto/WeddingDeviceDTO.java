package io.renren.modules.wedding.dto;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.renren.common.WeddingDeviceTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-09-09
 */
@Data
@ApiModel(value = "")
public class WeddingDeviceDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer id;

	@ApiModelProperty(value = "所属酒店")
	private String hotel;

	@ApiModelProperty(value = "婚期")
	private String time;

	@ApiModelProperty(value = "类型")
	private Integer type;

	@ApiModelProperty(value = "厅")
	private String office;

	@ApiModelProperty(value = "签单人")
	private String signer;

	@ApiModelProperty(value = "场布结构")
	private String structureLayout;

	@ApiModelProperty(value = "大屏/酒店音响")
	private String audioSystem;

	@ApiModelProperty(value = "司仪")
	private String masterIfCeremonies;

	@ApiModelProperty(value = "摄影/快剪")
	private String photograph;

	@ApiModelProperty(value = "摇臂/管家")
	private String housekeeper;

	@ApiModelProperty(value = "液氮")
	private String liquidNitrogen;

	@ApiModelProperty(value = "总金额")
	private String totalAmount;

	@ApiModelProperty(value = "定金")
	private String deposit;

	@ApiModelProperty(value = "中期款")
	private String interimPayment;

	@ApiModelProperty(value = "尾款")
	private String balancePayment;

	@ApiModelProperty(value = "软装人工费+督导")
	private String costForSoft;

	@ApiModelProperty(value = "新人信息")
	private String newcomerInformation;

	@ApiModelProperty(value = "新人电话")
	private String newcomerPhoneNumber;

	@ApiModelProperty(value = "备注")
	private String remark;

	private Long operatorId;

	private Date createTime;

	private Date updateTime;

}