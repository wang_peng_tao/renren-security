package io.renren.modules.wedding.service.impl;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.renren.common.page.PageData;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.ExcelUtil;
import io.renren.modules.security.user.SecurityUser;
import io.renren.modules.security.user.UserDetail;
import io.renren.modules.sys.dao.SysUserDao;
import io.renren.modules.sys.enums.SuperAdminEnum;
import io.renren.modules.sys.service.SysDeptService;
import io.renren.modules.wedding.dao.WeddingDeviceDao;
import io.renren.modules.wedding.dto.WeddingDeviceDTO;
import io.renren.modules.wedding.entity.WeddingDeviceEntity;
import io.renren.modules.wedding.excel.WeddingDeviceExcel;
import io.renren.modules.wedding.service.WeddingDeviceService;
import io.renren.modules.wedding.vo.req.WeddingDevicePageReqVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.shiro.spring.web.ShiroUrlPathHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-09-09
 */
@Service
@Slf4j
public class WeddingDeviceServiceImpl extends CrudServiceImpl<WeddingDeviceDao, WeddingDeviceEntity, WeddingDeviceDTO> implements WeddingDeviceService {

    @Autowired
    private SysDeptService sysDeptService;

    @Override
    public QueryWrapper<WeddingDeviceEntity> getWrapper(Map<String, Object> params) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startTime = null;
        Date timeEndTime = null;
        try {
            if (StringUtils.isNotBlank(params.get("timeStartTime").toString()) && StringUtils.isNotBlank(params.get("timeEndTime").toString())) {
                startTime = timeFormat.parse(params.get("timeStartTime").toString());
                timeEndTime = timeFormat.parse(params.get("timeEndTime").toString());
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        Integer type = null;
        if (StringUtils.isNotBlank(params.get("type").toString())) {
            type = Integer.valueOf(params.get("type").toString());
        }

        String signer = (String) params.get("signer");

        QueryWrapper<WeddingDeviceEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(ObjectUtil.isNotNull(type), "type", type);
        wrapper.like(StringUtils.isNotBlank(signer), "signer", signer);
        if (ObjectUtil.isNotNull(startTime) && ObjectUtil.isNotNull(timeEndTime)) {
            wrapper.between("time", startTime, timeEndTime);
        }
        return wrapper;
    }

    @Override
    public void importData(MultipartFile file) {
        //设置导入参数
        UserDetail user = SecurityUser.getUser();

        Workbook hssfWorkbook = null;
        try {
            hssfWorkbook = WorkbookFactory.create(file.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        int sheetNum = hssfWorkbook.getNumberOfSheets();
        for (int sheetIndex = 0; sheetIndex < sheetNum; sheetIndex++) {
            log.info("WeddingDeviceServiceImpl.importData 插入数据：sheetNum: " + sheetNum);
            String sheetName = hssfWorkbook.getSheetAt(sheetIndex).getSheetName();
            ImportParams param = new ImportParams();
            //标题行数
            param.setTitleRows(0);
            //表头行数
            param.setHeadRows(1);
            //读取第几个sheet页
            param.setStartSheetIndex(sheetIndex);
            //excel转POJO
            List<WeddingDeviceExcel> studentList = null;
            try {
                studentList = ExcelImportUtil.importExcel(file.getInputStream(),
                        WeddingDeviceExcel.class, param);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            List<WeddingDeviceEntity> insertList = new ArrayList<>();
            //添加到数据库
            for (WeddingDeviceExcel dto : studentList) {
                WeddingDeviceEntity weddingDeviceEntity = new WeddingDeviceEntity();
                BeanUtils.copyProperties(dto, weddingDeviceEntity);

                weddingDeviceEntity.setOperatorId(user.getId());
                weddingDeviceEntity.setCreateTime(new Date());
                weddingDeviceEntity.setUpdateTime(new Date());
                weddingDeviceEntity.setSheetName(sheetName);

                log.info("WeddingDeviceServiceImpl.importData 插入数据：" + JSONObject.toJSONString(weddingDeviceEntity));
                insertList.add(weddingDeviceEntity);
            }
            this.baseDao.insertBatch(insertList);
        }
    }

    @Override
    public PageData<WeddingDeviceDTO> getPage(WeddingDevicePageReqVo req) {
        UserDetail user = SecurityUser.getUser();

        PageHelper.startPage(req.getPage(), req.getLimit());
        LambdaQueryWrapper<WeddingDeviceEntity> lambdaQuery = Wrappers.lambdaQuery();
        Integer type = req.getType();
        String signer = req.getSigner();
        String time = req.getTime();

        lambdaQuery.eq(ObjectUtil.isNotNull(type), WeddingDeviceEntity::getType, type);
        lambdaQuery.like(StringUtils.isNotBlank(signer), WeddingDeviceEntity::getSigner, signer);
        lambdaQuery.like(StringUtils.isNotBlank(time), WeddingDeviceEntity::getTime, time);

        //非管理员只能查看自己及下属部门的数据
        if (user.getSuperAdmin() == SuperAdminEnum.NO.value()) {
            //获取下属部门用户，没有则只有自己
            List<Long> userList = sysDeptService.getDeptUserListByDeptId(user.getDeptId());
            userList.add(user.getId());
            lambdaQuery.in(WeddingDeviceEntity::getOperatorId, userList);
        }

        lambdaQuery.orderByDesc(WeddingDeviceEntity::getTime).orderByAsc(WeddingDeviceEntity::getOffice);

        List<WeddingDeviceEntity> weddingDeviceEntities = this.baseDao.selectList(lambdaQuery);
        PageInfo<WeddingDeviceEntity> pageInfo = new PageInfo<>(weddingDeviceEntities);

        List<WeddingDeviceEntity> list = pageInfo.getList();
        if (CollectionUtil.isNotEmpty(list)) {
            List<WeddingDeviceDTO> dtoList = new ArrayList<>();

            for (WeddingDeviceEntity weddingDeviceEntity : list) {
                WeddingDeviceDTO weddingDeviceDTO = new WeddingDeviceDTO();
                BeanUtils.copyProperties(weddingDeviceEntity, weddingDeviceDTO);
                dtoList.add(weddingDeviceDTO);
            }

            return new PageData<>(dtoList, pageInfo.getTotal());
        }

        return new PageData<>(new ArrayList<>(), 0);
    }

    @Override
    public List<WeddingDeviceDTO> getExportList(Map<String, Object> params) {
        Date startTime = null;
        Date timeEndTime = null;
        if (StringUtils.isNotBlank(params.get("timeStartTime").toString()) && StringUtils.isNotBlank(params.get("timeEndTime").toString())) {
            startTime = DateUtil.parseDateTime(params.get("timeStartTime").toString());
            timeEndTime = DateUtil.parseDateTime(params.get("timeEndTime").toString());
        }

        Integer type = null;
        if (StringUtils.isNotBlank(params.get("type").toString())) {
            type = Integer.valueOf(params.get("type").toString());
        }

        String signer = (String) params.get("signer");

        LambdaQueryWrapper<WeddingDeviceEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ObjectUtil.isNotNull(type), WeddingDeviceEntity::getType, type);
        wrapper.like(StringUtils.isNotBlank(signer), WeddingDeviceEntity::getSigner, signer);
        if (ObjectUtil.isNotNull(startTime) && ObjectUtil.isNotNull(timeEndTime)) {
            wrapper.between(WeddingDeviceEntity::getTime, startTime, timeEndTime);
        }

        wrapper.orderByDesc(WeddingDeviceEntity::getTime).orderByAsc(WeddingDeviceEntity::getOffice);

        List<WeddingDeviceEntity> weddingDeviceEntities = this.baseDao.selectList(wrapper);

        List<WeddingDeviceDTO> list = new ArrayList<>();
        for (WeddingDeviceEntity weddingDeviceEntity : weddingDeviceEntities) {
            WeddingDeviceDTO weddingDeviceDTO = new WeddingDeviceDTO();
            BeanUtils.copyProperties(weddingDeviceEntity, weddingDeviceDTO);
            list.add(weddingDeviceDTO);
        }
        return list;
    }

    @Override
    public void exportExcel(Map<String, Object> params, HttpServletResponse response) {
        UserDetail user = SecurityUser.getUser();

        List<Long> userList = null;
        //非管理员只能查看自己及下属部门的数据
        if (user.getSuperAdmin() == SuperAdminEnum.NO.value()) {
            //获取下属部门用户，没有则只有自己
            userList = sysDeptService.getDeptUserListByDeptId(user.getDeptId());
            userList.add(user.getId());
        }
        List<String> sheetNameList = this.baseDao.getSheetName(userList);

        LambdaQueryWrapper<WeddingDeviceEntity> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (CollectionUtil.isNotEmpty(sheetNameList)) {
            lambdaQueryWrapper.in(WeddingDeviceEntity::getSheetName, sheetNameList);
        } else {
            lambdaQueryWrapper.isNull(WeddingDeviceEntity::getSheetName);
        }

        Integer type = null;
        if (StringUtils.isNotBlank(params.get("type").toString())) {
            type = Integer.valueOf(params.get("type").toString());
        }

        String signer = (String) params.get("signer");
        String time = (String) params.get("time");

        lambdaQueryWrapper.eq(ObjectUtil.isNotNull(type), WeddingDeviceEntity::getType, type);
        lambdaQueryWrapper.like(StringUtils.isNotBlank(signer), WeddingDeviceEntity::getSigner, signer);
        lambdaQueryWrapper.like(StringUtils.isNotBlank(time), WeddingDeviceEntity::getTime, time);
        //空的也计算
        lambdaQueryWrapper.or(e -> e.isNull(WeddingDeviceEntity::getSheetName));

        List<WeddingDeviceEntity> weddingDeviceEntities = this.baseDao.selectList(lambdaQueryWrapper);

        List<Map<String, Object>> sheetsList = new ArrayList<>();
        Workbook workbook;
        if (CollectionUtil.isEmpty(weddingDeviceEntities)) {
            workbook = ExcelExportUtil.exportExcel(sheetsList, ExcelType.HSSF);
            ExcelUtil.downLoadExcel("汇总表.xls", response, workbook);
        }

        Map<String, List<WeddingDeviceExcel>> sheetNameToList = weddingDeviceEntities.stream().map(item -> {
            WeddingDeviceExcel weddingDeviceExcel = new WeddingDeviceExcel();
            BeanUtils.copyProperties(item,weddingDeviceExcel);
            return weddingDeviceExcel;
        }).collect(Collectors.groupingBy(WeddingDeviceExcel::getSheetName));


        for (String sheetName : sheetNameList) {
            List<WeddingDeviceExcel> dataList = sheetNameToList.get(sheetName);
            if (CollectionUtil.isNotEmpty(dataList)) {
                // 创建参数对象
                ExportParams exportParams = new ExportParams();
                // 设置sheet得名称
                exportParams.setSheetName(sheetName);

                // 创建sheet1使用得map
                Map<String, Object> deptDataMap = new HashMap<>(4);
                // title的参数为ExportParams类型
                deptDataMap.put("title", exportParams);
                // 模版导出对应得实体类型
                deptDataMap.put("entity", WeddingDeviceExcel.class);
                // sheet中要填充得数据
                deptDataMap.put("data", dataList);

                // 将sheet1和sheet2使用得map进行包装
                sheetsList.add(deptDataMap);
            }
        }

        workbook = ExcelExportUtil.exportExcel(sheetsList, ExcelType.HSSF);
        // 执行方法
        ExcelUtil.downLoadExcel("汇总表.xls", response, workbook);
    }
}