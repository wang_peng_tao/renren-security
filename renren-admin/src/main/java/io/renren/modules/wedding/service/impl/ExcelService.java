//package io.renren.modules.wedding.service.impl;
//
//import cn.afterturn.easypoi.excel.ExcelExportUtil;
//import cn.afterturn.easypoi.excel.ExcelImportUtil;
//import cn.afterturn.easypoi.excel.entity.ExportParams;
//import cn.afterturn.easypoi.excel.entity.ImportParams;
//import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
//import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
//import com.gourd.common.data.BaseResponse;
//import com.gourd.common.rbac.entity.RbacDepart;
//import com.gourd.common.rbac.entity.RbacUser;
//import com.gourd.common.rbac.service.RbacUserService;
//import com.gourd.common.utils.CollectionCopyUtil;
//import com.gourd.common.utils.DateUtil;
//import com.gourd.common.utils.ExcelUtil;
//import com.gourd.file.excel.entity.DeptExcel;
//import com.gourd.file.excel.entity.UserExcel;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.poi.ss.usermodel.Workbook;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.*;
//
///**
// * @author gourd
// */
//@Service
//@Slf4j
//public class ExcelService {
//
//
//    @Autowired
//    private RbacUserService userService;
//
//    /**
//     * 单sheet导出
//     *
//     * @param response
//     */
//    public void export(HttpServletResponse response) {
//        // 模拟从数据库获取需要导出的数据
//        List<RbacUser> personList = userService.findAll();
//        List<UserExcel> userExcelList =  CollectionCopyUtil.copyList(personList,UserExcel.class);
//        //导出操作
//        ExcelUtil.exportExcel(userExcelList,"用户列表","用户", UserExcel.class,"用户信息"+ DateUtil.date2Str(new Date(),"YYYY-MM-dd-HH-mm") +".xls",response);
//
//    }
//
//    /**
//     * 多sheet导出示例
//     *
//     * @return
//     */
//    public void exportSheets(HttpServletResponse response) {
//        // 准备测试数据
//        List<UserExcel> userExcels = new ArrayList<>();
//        UserExcel userExcel = new UserExcel();
//        userExcel.setName("gourd");
//        userExcel.setAge(23);
//        userExcel.setSex("M");
//        userExcel.setMobilePhone("10086");
//        userExcel.setEmail("163.com");
//        userExcel.setNickName("gourd");
//        userExcel.setBirth(new Date());
//        userExcels.add(userExcel);
//        UserExcel userExcel2 = new UserExcel();
//        userExcel2.setName("葫芦");
//        userExcel2.setAge(23);
//        userExcel2.setSex("X");
//        userExcel2.setMobilePhone("110");
//        userExcel2.setEmail("136@163.com");
//        userExcel2.setNickName("gourd");
//        userExcel2.setBirth(new Date());
//        userExcels.add(userExcel2);
//        List<DeptExcel> deptExcels = new ArrayList<>();
//        DeptExcel deptExcel = new DeptExcel();
//        deptExcel.setCode("T-01");
//        deptExcel.setName("技术一部");
//        deptExcel.setUsers(userExcels);
//        deptExcels.add(deptExcel);
//        DeptExcel deptExcel2 = new DeptExcel();
//        deptExcel2.setCode("T-02");
//        deptExcel2.setName("技术二部");
//        deptExcel2.setUsers(userExcels);
//        deptExcels.add(deptExcel2);
//
//        // 创建参数对象
//        ExportParams exportParams1 = new ExportParams();
//        // 设置sheet得名称
//        exportParams1.setSheetName("部门人员信息");
//        ExportParams exportParams2 = new ExportParams();
//        exportParams2.setSheetName("用户信息");
//
//        // 创建sheet1使用得map
//        Map<String, Object> deptDataMap = new HashMap<>(4);
//        // title的参数为ExportParams类型
//        deptDataMap.put("title", exportParams1);
//        // 模版导出对应得实体类型
//        deptDataMap.put("entity", DeptExcel.class);
//        // sheet中要填充得数据
//        deptDataMap.put("data", deptExcels);
//
//        // 创建sheet2使用得map
//        Map<String, Object> userDataMap = new HashMap<>(4);
//        userDataMap.put("title", exportParams2);
//        userDataMap.put("entity", UserExcel.class);
//        userDataMap.put("data", userExcels);
//        // 将sheet1和sheet2使用得map进行包装
//        List<Map<String, Object>> sheetsList = new ArrayList<>();
//        sheetsList.add(deptDataMap);
//        sheetsList.add(userDataMap);
//        // 执行方法
//        Workbook workbook = ExcelExportUtil.exportExcel(sheetsList, ExcelType.HSSF);
//        ExcelUtil.downLoadExcel("用户信息"+ DateUtil.date2Str(new Date(),"YYYY-MM-dd-HH-mm") +".xls",response,workbook);
//    }
//
//    /**
//     * 单sheet导入
//     *
//     * @param file
//     */
//    public BaseResponse importExcel(MultipartFile file) {
//        ImportParams importParams = new ImportParams();
//        // 距离表头中间有几行不要的数据
//        importParams.setHeadRows(1);
//        // 表头在第几行
//        importParams.setTitleRows(1);
//        // 需要验证
//        importParams.setNeedVerfiy(true);
//        try {
//            ExcelImportResult<UserExcel> result = ExcelImportUtil.importExcelMore(file.getInputStream(), UserExcel.class,
//                    importParams);
//            List<UserExcel> successList = result.getList();
//            for (UserExceldemoExcel : successList) {
//                System.out.println(demoExcel);
//            }
//        }catch (Exception e) {
//            log.error("导入失败:{}",e);
//            return BaseResponse.failure("导入失败！请检查导入文档的格式是否正确");
//        }
//        return BaseResponse.ok("导入成功");
//    }
//
//
//    /**
//     * 多sheet导入
//     *
//     * @param file
//     * @return
//     * @throws IOException
//     */
//    public BaseResponse importSheets(MultipartFile file){
//        try {
//            // 根据file得到Workbook,主要是要根据这个对象获取,传过来的excel有几个sheet页
//            Workbook workBook = ExcelUtil.getWorkBook(file);
//            StringBuilder sb=new StringBuilder();
//            ImportParams params = new ImportParams();
//            // 循环工作表Sheet
//            for (int numSheet = 0; numSheet < workBook.getNumberOfSheets(); numSheet++) {
//                // 表头在第几行
//                params.setTitleRows(0);
//                // 距离表头中间有几行不要的数据
//                params.setStartRows(1);
//                // 第几个sheet页
//                params.setStartSheetIndex(numSheet);
//                // 验证数据
//                params.setNeedVerfiy(true);
//                if(numSheet==0){
//                    ExcelImportResult<UserExcel> result =ExcelImportUtil.importExcelMore(file.getInputStream(),
//                            UserExcel.class, params);
//                    // 校验是否合格
//                    if(result.isVerfiyFail()){
//                        // 不合格的数据
//                        List<UserExcel> errorList = result.getList();
//                        // 拼凑错误信息,自定义
//                        for(int i=0;i<errorList.size();i++){
//                            ExcelUtil.getWrongInfo(sb, errorList, i, errorList.get(i), "name", "用户信息不合法");
//                        }
//                    }
//                    // 合格的数据
//                    List<UserExcel > successList = result.getList();
//                    // 业务逻辑
//
//                }else if(numSheet==1){
//                    ExcelImportResult<DeptExcel> result =ExcelImportUtil.importExcelMore(file.getInputStream(),
//                            DeptExcel.class, params);
//                    // 校验是否合格
//                    if(result.isVerfiyFail()){
//                        // 不合格的数据
//                        List<RbacDepart> errorList = result.getList();
//                        // 拼凑错误信息,自定义
//                        for(int i=0;i<errorList.size();i++){
//                            ExcelUtil.getWrongInfo(sb, errorList, i, errorList.get(i), "name", "部门信息不合法");
//                        }
//                    }
//                    // 校验合格的数据
//                    List<DeptExcel> successList = result.getList();
//                    // 业务逻辑
//                }
//
//            }
//        } catch (Exception e) {
//            log.error("导入失败:{}",e);
//            return BaseResponse.failure("导入失败！请检查导入文档的格式是否正确");
//        }
//        return BaseResponse.ok("导入成功！");
//    }
//
//}