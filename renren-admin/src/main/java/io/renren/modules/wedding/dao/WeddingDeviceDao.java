package io.renren.modules.wedding.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.wedding.entity.WeddingDeviceEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-09-09
 */
@Mapper
public interface WeddingDeviceDao extends BaseDao<WeddingDeviceEntity> {

    void insertBatch(@Param("list") List<WeddingDeviceEntity> insertList);

    List<String> getSheetName(@Param("userIdList") List<Long> userList);
}