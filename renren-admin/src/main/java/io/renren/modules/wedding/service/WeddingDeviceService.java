package io.renren.modules.wedding.service;

import io.renren.common.page.PageData;
import io.renren.common.service.CrudService;
import io.renren.modules.wedding.dto.WeddingDeviceDTO;
import io.renren.modules.wedding.entity.WeddingDeviceEntity;
import io.renren.modules.wedding.vo.req.WeddingDevicePageReqVo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-09-09
 */
public interface WeddingDeviceService extends CrudService<WeddingDeviceEntity, WeddingDeviceDTO> {

    void importData(MultipartFile file);

    /**
     * 分页获取数据
     * @param req
     * @return
     */
    PageData<WeddingDeviceDTO> getPage(WeddingDevicePageReqVo req);

    List<WeddingDeviceDTO> getExportList(Map<String, Object> params);

    void exportExcel(Map<String, Object> params, HttpServletResponse response);
}