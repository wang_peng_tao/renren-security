package io.renren.modules.wedding.vo.req;

import io.renren.common.vo.BasePageReqVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Description TODO
 * @Author PengTao Wang
 * @Date 2023/9/10 23:17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class WeddingDevicePageReqVo extends BasePageReqVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String signer;

    private Integer type;

    private String time;
}
