package io.renren.common.vo;

import lombok.Data;

/**
 * @Description TODO
 * @Author PengTao Wang
 * @Date 2023/9/10 23:18
 */
@Data
public class BasePageReqVo {
    private Integer page;

    private Integer limit;
}
