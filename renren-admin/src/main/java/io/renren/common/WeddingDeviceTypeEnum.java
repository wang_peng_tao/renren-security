package io.renren.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description 类型–午宴/晚宴
 * @Author PengTao Wang
 * @Date 2023/9/10 22:19
 */
@AllArgsConstructor
@NoArgsConstructor
public enum WeddingDeviceTypeEnum {
    LunchBanquet(0,"午宴"),
    dinner(0,"晚宴");
    private Integer type;
    private String name;
}
