/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.common.utils;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.BeanUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.*;

/**
 * excel工具类
 *
 * @author Mark sunlightcs@gmail.com
 */
public class ExcelUtils {

    /**
     * Excel导出
     *
     * @param response      response
     * @param fileName      文件名
     * @param list          数据List
     * @param pojoClass     对象Class
     */
    public static void exportExcel(HttpServletResponse response, String fileName, Collection<?> list,
                                     Class<?> pojoClass) throws IOException {
        if(StringUtils.isBlank(fileName)){
            //当前日期
            fileName = DateUtils.format(new Date());
        }

        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(), pojoClass, list);
        response.setCharacterEncoding("UTF-8");
        response.setHeader("content-Type", "application/vnd.ms-excel");
        response.setHeader("Content-Disposition",
                "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8") + ".xls");
        ServletOutputStream out = response.getOutputStream();
        workbook.write(out);
        out.flush();
    }

    /**
     * Excel导出，先sourceList转换成List<targetClass>，再导出
     *
     * @param response      response
     * @param fileName      文件名
     * @param sourceList    原数据List
     * @param targetClass   目标对象Class
     */
    public static void exportExcelToTarget(HttpServletResponse response, String fileName, Collection<?> sourceList,
                                     Class<?> targetClass) throws Exception {
        List targetList = new ArrayList<>(sourceList.size());
        for(Object source : sourceList){
            Object target = targetClass.newInstance();
            BeanUtils.copyProperties(source, target);
            targetList.add(target);
        }

        exportExcel(response, fileName, targetList, targetClass);
    }


//    public <T> List<T> readExcel(File file, Class<T> clazz) throws Exception {
//        List<T> list = new ArrayList<>();
//        try (Workbook workbook = WorkbookFactory.create(file)) {
//            Sheet sheet = workbook.getSheetAt(0);
//            Iterator<Row> iterator = sheet.iterator();
//            Map<Integer, String> columnMap = new HashMap<>();
//            while (iterator.hasNext()) {
//                Row row = iterator.next();
//                if (row.getRowNum() == 0) {
//                    Iterator<Cell> cellIterator = row.cellIterator();
//                    while (cellIterator.hasNext()) {
//                        Cell cell = cellIterator.next();
//                        columnMap.put(cell.getColumnIndex(), cell.getStringCellValue());
//                    }
//                } else {
//                    T obj = clazz.newInstance();
//                    Iterator<Cell> cellIterator = row.cellIterator();
//                    while (cellIterator.hasNext()) {
//                        Cell cell = cellIterator.next();
//                        int columnIndex = cell.getColumnIndex();
//                        String columnName = columnMap.get(columnIndex);
//                        Field field = clazz.getDeclaredField(columnName);
//                        field.setAccessible(true);
//                        if (field.isAnnotationPresent(ExcelColumn.class)) {
//                            String cellValue = getCellValue(cell);
//                            Class<?> fieldType = field.getType();
//                            if (fieldType == String.class) {
//                                field.set(obj, cellValue);
//                            } else if (fieldType == Integer.class) {
//                                field.set(obj, Integer.valueOf(cellValue));
//                            } else if (fieldType == Date.class) {
//                                field.set(obj, new SimpleDateFormat("yyyy-MM-dd").parse(cellValue));
//                            }
//                        }
//                    }
//                    list.add(obj);
//                }
//            }
//        }
//        return list;
//    }
}
