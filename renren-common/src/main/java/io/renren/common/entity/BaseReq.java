package io.renren.common.entity;

import lombok.Data;

/**
 * @Description TODO
 * @Author PengTao Wang
 * @Date 2023/9/10 23:18
 */
@Data
public class BaseReq {
    private Integer pageNo;

    private Integer limit;

    private Integer pageSize;
}
